import express from "express";

export const router = express.Router();
export default { router };

router.get("/", (req, res) => {
	const params = {
		nombre: req.query.nombre,
		lvl: req.query.lvl,
		pagoBase: req.query.pagoBase,
		horas: req.query.horas,
		cantidadHijos: req.query.cantidadHijos
	};
	res.render("pago", params);
});

router.post("/", (req, res) => {
	const params = {
		nombre: req.body.nombre,
		lvl: req.body.lvl,
		pagoBase: req.body.pagoBase,
		horas: req.body.horas,
		cantidadHijos: req.body.cantidadHijos
	};
	res.render("tabla", params);
});